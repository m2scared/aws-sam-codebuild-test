import json
import ulid


def lambda_handler(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps({
            "ulid": str(ulid.ULID()),
            "message": "hello world",
        }),
    }
